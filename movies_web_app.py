import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


@app.route('/add_movie', methods=['POST'])
def add_movie():
    create_table()
    year = request.form['year']
    title = str(request.form['title']).lower()
    director = request.form['director']
    actor = str(request.form['actor']).lower()
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    message = "Movie %s successfully inserted" % title
    try:
        cur = cnx.cursor()
        sql = "SELECT * FROM movies WHERE title = ('" + title + "')"
        cur.execute(sql)
        if len(cur.fetchall()) > 0:
            message = "Movie %s already exists, try updating it instead" % title
        else:
            sql = "INSERT INTO movies (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)"
            val = (year, title, director, actor, release_date, rating)
            cur.execute(sql, val)
            cnx.commit()
    except mysql.connector.Error as err:
        message = "Movie %s could not be inserted - %s" % (title, err)

    return render_template('index.html', message=message)


@app.route('/update_movie', methods=['POST'])
def update_movie():
    year = request.form['year']
    title = str(request.form['title']).lower()
    director = request.form['director']
    actor = str(request.form['actor']).lower()
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    message = "Movie %s successfully updated" % title
    try:
        cur = cnx.cursor()
        sql = "SELECT * FROM movies WHERE title = ('" + title + "')"
        cur.execute(sql)
        if len(cur.fetchall()) == 0:
            message = "Movie with %s does not exist" % title
        else:
            sql = "UPDATE movies SET year = %s, director = %s, actor = %s, release_date = %s, rating = %s WHERE title = %s"
            val = (year, director, actor, release_date, rating, title)
            cur.execute(sql, val)
            cnx.commit()
    except mysql.connector.Error as err:
        message = "Movie %s could not be updated - %s" % (title, err)

    return render_template('index.html', message=message)


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    delete_title = str(request.form['delete_title']).lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    message = "Movie %s successfully deleted" % delete_title
    try:
        cur = cnx.cursor()
        sql = "SELECT * FROM movies WHERE title = ('" + delete_title + "')"
        cur.execute(sql)
        if len(cur.fetchall()) == 0:
            message = "Movie with %s does not exist" % delete_title
        else:
            sql = "DELETE FROM movies WHERE title = ('" + delete_title + "')"
            cur.execute(sql)
            cnx.commit()
    except mysql.connector.Error as err:
        message = "Movie %s could not be deleted - %s" % (delete_title, err)

    return render_template('index.html', message=message)


@app.route('/search_movie', methods=['GET'])
def search_movie():
    search_actor = str(request.args['search_actor']).lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT * FROM movies WHERE LOWER(actor) = ('" + search_actor + "')"
    cur.execute(sql)
    result = cur.fetchall()
    if len(result) > 0:
        new_message = ""
        for movie in result:
            new_message += ("%s, %s, %s" % (movie[2], movie[1], movie[4])) + '<br>'
    else:
        new_message = "No movies found for actor %s" % search_actor

    return render_template('index.html', message=new_message)


@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT * FROM movies ORDER BY rating DESC"
    cur.execute(sql)
    result = cur.fetchall()
    if len(result) > 0:
        movie = result[0]
        top_rating = str(movie[6])
        sql = 'SELECT * FROM movies WHERE rating = ' + top_rating
        cur.execute(sql)
        result = cur.fetchall()
        new_message = ''
        for movie in result:
            new_message += ("%s, %s, %s, %s, %s" % (movie[2], movie[1], movie[4], movie[3], top_rating)) + '<br>'
    else:
        new_message = 'No movies in database'
    return render_template('index.html', message=new_message)


@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT * FROM movies ORDER BY rating"
    cur.execute(sql)
    result = cur.fetchall()
    if len(result) > 0:
        movie = result[0]
        bot_rating = str(movie[6])
        sql = 'SELECT * FROM movies WHERE rating = ' + bot_rating
        cur.execute(sql)
        result = cur.fetchall()
        new_message = ''
        for movie in result:
            new_message += ("%s, %s, %s, %s, %s" % (movie[2], movie[1], movie[4], movie[3], bot_rating)) + '<br>'
    else:
        new_message = 'No movies in database'
    return render_template('index.html', message=new_message)


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_to_db', methods=['POST'])
def add_to_db():
    print("Received request.")
    print(request.form['message'])
    msg = request.form['message']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
    cnx.commit()
    return hello()


@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    #entries = query_data()
    #print("Entries: %s" % entries)
    return render_template('index.html')


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
